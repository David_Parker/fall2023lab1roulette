import java.util.Random;

public class RouletteWheel {    
    //Fields
    private Random random;
    private int number;

    //Constructor
    public RouletteWheel() {
        Random rand = new Random();
        this.random = rand;
        this.number = 0;
    }

    //Custom
    public void spin() {
        this.number = this.random.nextInt(37);
    }

    //Getter methods
    public int getNumber() {
        return this.number;
    }
}
