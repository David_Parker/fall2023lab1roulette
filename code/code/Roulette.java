import java.util.Scanner;

public class Roulette {

    //Returns true or false from a yes or no answer
    public static boolean booleanRequests(String request) {
        Scanner reader = new Scanner(System.in);
        boolean hasAnswered = false;
        String answer = "";

        while (!hasAnswered) {
            System.out.println(request);
            answer = reader.nextLine();
            if (answer.equalsIgnoreCase("Y") || answer.equalsIgnoreCase("N")) {
                hasAnswered = true;
            }
            else {
                System.out.println("Invalid input. Answer again.");
            }
        }
        return answer.equalsIgnoreCase("Y");
    }

    //Returns the amount of money bet by the player
    public static int placeBet(int money) {
        Scanner reader = new Scanner(System.in);
        int betAmount = 0;

        while(betAmount <= 0 || betAmount > money) {
            System.out.println("How much would you like to bet?");
            betAmount = Integer.parseInt(reader.nextLine());
            if (betAmount <= 0 || betAmount > money) {
                System.out.println("Bet is not valid. Re-input.");
            }
        }
        return betAmount;
    }

    //Returns the number the player bets on
    public static int numberToBetOn() {
        Scanner reader = new Scanner(System.in);
        int numberBetOn = -1;

        while(numberBetOn < 0 || numberBetOn > 36) {
            System.out.println("Which number would you like to bet on?");
            numberBetOn = Integer.parseInt(reader.nextLine());
            if (numberBetOn < 0 || numberBetOn > 36) {
                System.out.println("Number must be between 0-36.");
            }
        }
        return numberBetOn;
    }

    public static boolean hasWon(int wheelNumber, int userNumber) {
        return wheelNumber == userNumber;
    }

    //Playing through a round of roulette
    public static int playthrough(int money) {
        boolean isBetting = booleanRequests("Would you like to place a bet? (y/n)");
        int userNumber = 0;
        int betAmount = 0;
        RouletteWheel wheel = new RouletteWheel();
        boolean hasWon = false;
        
        //Enters if the player has chosen to bet
        if(isBetting) {
            userNumber = numberToBetOn();
            betAmount = placeBet(money);
            System.out.println("Bets closed.\nSpinning wheel...");
            wheel.spin();
            System.out.println("The dolly has landed on " + wheel.getNumber());
            hasWon = hasWon(wheel.getNumber(), userNumber);
            if (hasWon) {
                return betAmount*35;
            }
            else {
                return betAmount*-1;
            }
        }

        //Returns 0 if they didnt play
        return 0;
    }

    public static void main(String[] args) {
        int userMoney = 1000;
        int payout = 0;
        int totalEarnings = 0;
        boolean playAgain = true;
        
        while(playAgain) {
            payout = playthrough(userMoney);
            totalEarnings += payout;

            System.out.println("Congratulations to the winners.\nYou have won $" + payout);
            playAgain = booleanRequests("Would you like to play again?");
        }

        System.out.println("Thank you for playing. You have earned: $" + totalEarnings);
    }
}